import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

import java.util.List;

import static org.testng.Assert.assertEquals;

public class DemoPageObject {

    WebDriver driver = new BasePageObject().getDriver();
    private By inputName = By.xpath("//input[@name=\"name\"]");
    private By inputPhoneNumber = By.xpath("//input[@name=\"phoneNumber\"]");
    private By buttonAddRecord = By.xpath("//button[text()=\" Add record \"]");
    private By inputFilter = By.xpath("//input[@name=\"filter\"]");
    private By buttonFilter = By.xpath("//button[text()=\"Filter\"]");
    private By buttonDelete = By.xpath("./td/button[text()=\" Delete record\"]");

    public static void checkCellEqualTo(List<WebElement> rows, int rowNum, String name, String phoneNumber) {
        String cell11 = rows.get(rowNum).findElements(By.xpath("./td")).get(0).getText();
        String cell12 = rows.get(rowNum).findElements(By.xpath("./td")).get(1).getText();

        assertEquals(cell11, name);
        assertEquals(cell12, phoneNumber);
    }

    public void navigateTo(String url) {
        driver.get(url);
    }

    public void driverQuit() {
        driver.quit();
    }

    public void deleteRecord(List<WebElement> rows, int rowNum) {
        rows.get(rowNum).findElement(buttonDelete).click();
    }

    public List<WebElement> checkResultTable(int exspectedRownsNum) {
        List<WebElement> rows = driver.findElements(By.xpath("//table/tbody/tr"));
        assertEquals(rows.size(), exspectedRownsNum);
        return rows;
    }

    public List<WebElement> getRowsFromTable() {
        List<WebElement> rows = driver.findElements(By.xpath("//table/tbody/tr"));
        return rows;
    }

    public void filterByName(String filter) {
        setFilter(inputFilter, filter);
        clickButton(buttonFilter);
    }

    public void setFilter(By locator, String testName2) {
        typeInToField(inputFilter, testName2);
    }

    public void addRecord(String testName1, String s) {
        setName(testName1);
        setPhoneNumber(s);
        clickButton(buttonAddRecord);
    }

    public void clickButton(By locator) {
        driver.findElement(locator).click();
    }

    public void setName(String testName1) {
        typeInToField(inputName, testName1);
    }

    public void setPhoneNumber(String testName1) {
        typeInToField(inputPhoneNumber, testName1);
    }

    public void typeInToField(By locator, String testName1) {
        driver.findElement(locator).clear();
        driver.findElement(locator).sendKeys(testName1);
    }

}
