import io.github.bonigarcia.wdm.ChromeDriverManager;
import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import static io.github.bonigarcia.wdm.DriverManagerType.CHROME;

public class BasePageObject {

    public WebDriver driver;


    public WebDriver initialDriver() {
        WebDriverManager.chromedriver().setup();
        ChromeDriverManager.getInstance(CHROME).setup();

        driver = new ChromeDriver();
        return driver;
    }

    public WebDriver getDriver() {
        if (driver == null) {
            driver = initialDriver();
            return driver;
        } else {
            return driver;
        }
    }


}
