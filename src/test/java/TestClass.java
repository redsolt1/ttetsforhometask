import org.openqa.selenium.WebElement;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import java.util.List;

public class TestClass {

    String name1 = "testName1";
    String name2 = "testName2";
    String name3 = "testName3";
    String phoneNumber1 = "123-45-671";
    String phoneNumber2 = "123-45-672";
    String phoneNumber3 = "123-45-673";
    private DemoPageObject demoPageObject = new DemoPageObject();


//    @BeforeClass
//    public void beforeClass() {
//        WebDriverManager.chromedriver().setup();
//    }
//
//    @BeforeMethod
//    public void before () {
//        driver = new ChromeDriver();
//    }

    @BeforeTest
    public void navigateAndClear() {
        demoPageObject.navigateTo("http://localhost:8080/");
        demoPageObject.filterByName("");
        while (demoPageObject.getRowsFromTable().size() > 0) {

            demoPageObject.deleteRecord(demoPageObject.getRowsFromTable(), 0);
        }

    }

    @Test
    public void happyPathTest() {

        demoPageObject.addRecord(name1, phoneNumber1);
        demoPageObject.addRecord(name2, phoneNumber2);
        demoPageObject.addRecord(name3, phoneNumber3);

        demoPageObject.filterByName(name2);

        List<WebElement> rows = demoPageObject.checkResultTable(1);

        DemoPageObject.checkCellEqualTo(rows, 0, name2, phoneNumber2);

        demoPageObject.filterByName("");
        rows = demoPageObject.checkResultTable(3);

        demoPageObject.deleteRecord(rows, 1);

        rows = demoPageObject.checkResultTable(2);

        DemoPageObject.checkCellEqualTo(rows, 0, name1, phoneNumber1);
        DemoPageObject.checkCellEqualTo(rows, 1, name3, phoneNumber3);
    }


    @AfterClass
    public void destroy() {
        demoPageObject.driverQuit();
    }


}
